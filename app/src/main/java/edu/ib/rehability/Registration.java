package edu.ib.rehability;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Registration extends AppCompatActivity {

    private Button btnRegistration;
    private TextView btnLoginIfNoAcc;
    private EditText emailET, password1ET, password2ET;
    private FirebaseAuth mAuth;
    private TextView tvNoMatchingPass;
    private String email, password1, password2, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        btnRegistration = findViewById(R.id.btnRegistration);
        btnLoginIfNoAcc = findViewById(R.id.tvLoginIfNoAcc);

        tvNoMatchingPass = findViewById(R.id.tvNotMatchingPassword);

        mAuth = FirebaseAuth.getInstance();

        initListeners();
    }

    public void initListeners() {

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            private static final String TAG = "HEHE";

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

                emailET = findViewById(R.id.etEmailRegistration);
                password1ET = findViewById(R.id.etPassword1Registration);
                password2ET = findViewById(R.id.etPassword2Registration);
                email = emailET.getText().toString();
                password1 = password1ET.getText().toString();
                password2 = password2ET.getText().toString();
                FirebaseUser user = mAuth.getCurrentUser();
                if(password1.equals(password2)) {
                    password = password1;
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(Registration.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Toast.makeText(Registration.this, "Rejestracja przebiegła pomyślnie.",
                                                Toast.LENGTH_SHORT).show();
                                        //FirebaseUser user = mAuth.getCurrentUser();

                                    }else if(user!=null){
                                        Toast.makeText(Registration.this, "Użytkownik o takim adresie email ma już konto.",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(Registration.this, "Błąd rejestracji.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }else{
                    tvNoMatchingPass.setText("Hasła muszą być takie same!");
                }
            }
        });


        btnLoginIfNoAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoggingIn.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }



}